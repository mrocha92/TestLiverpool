package com.example.testliverpool.usescases.usescases

import androidx.paging.*
import com.example.testliverpool.local.products.RecordDetailModel
import com.example.testliverpool.network.entities.RecordModel
import com.example.testliverpool.network.repository.ProductsRepository
import com.example.testliverpool.usescases.datasources.ProductsPagingSource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class GetProductsPagingSource {
    val productsRepository = ProductsRepository()
    operator fun invoke(query: String, viewModelScope: CoroutineScope): Flow<PagingData<RecordDetailModel>> {
        return Pager(
            PagingConfig(initialLoadSize = 10, pageSize = 56)
        ) {
            ProductsPagingSource(query, productsRepository)
        }.flow
            .map { pagingData ->
                pagingData.map { applicationDetail ->
                    applicationDetail.run {
                        RecordDetailModel(
                            RecordModel(
                                category = category,
                                groupType = groupType,
                                lgImage = lgImage,
                                listPrice = listPrice,
                                minimumListPrice = minimumListPrice,
                                minimumPromoPrice = minimumPromoPrice,
                                maximumPromoPrice = maximumPromoPrice,
                                productDisplayName = productDisplayName,
                                promoPrice = promoPrice,
                                smImage = smImage,
                                variantsColor = variantsColor,
                                xlImage = xlImage,
                            )
                        )
                    }
                }
            }
            .cachedIn(viewModelScope)
    }
}