package com.example.testliverpool.usescases.datasources

import android.util.Log
import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.example.testliverpool.network.entities.RecordModel
import com.example.testliverpool.network.repository.ProductsRepository

class ProductsPagingSource constructor(private val query: String, private val applicationsRepository: ProductsRepository) :
    PagingSource<Int, RecordModel>() {

    override fun getRefreshKey(state: PagingState<Int, RecordModel>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            val anchorPage = state.closestPageToPosition(anchorPosition)
            anchorPage?.prevKey?.plus(1) ?: anchorPage?.nextKey?.minus(1)
        }
    }

    override suspend fun load( params: LoadParams<Int>): LoadResult<Int, RecordModel> {
        return try {
            val nextPageNumber = params.key ?: 1
            val response = applicationsRepository.getProducts(query , nextPageNumber.toString())
            if (response.code() != 200) {
                LoadResult.Error(Exception(response.message() ?: "An error occurred fetching data"))
            } else {
                LoadResult.Page(
                    data = response.body()?.plpResults?.records ?: listOf(),
                    prevKey = null,
                    nextKey = if (nextPageNumber < (response.body()?.plpResults?.plpState?.lastRecNum  ?: -1)) nextPageNumber + 1 else null
                )
            }
        } catch (e: Exception) {
            Log.e("PagingError", e.message ?: "")
            LoadResult.Error(e)
        }
    }
}