package com.example.testliverpool.local.products

import androidx.recyclerview.widget.DiffUtil
import com.example.testliverpool.network.entities.RecordModel

class RecordDetailModel(private val _productInfo: RecordModel? = null) {
    val productInfo: RecordModel
        get() = _productInfo ?: RecordModel()

    class Comparator : DiffUtil.ItemCallback<RecordDetailModel>() {
        override fun areItemsTheSame(
            oldItem: RecordDetailModel,
            newItem: RecordDetailModel
        ): Boolean {
            return oldItem.productInfo.category == newItem.productInfo.category &&
                    oldItem.productInfo.productDisplayName == oldItem.productInfo.productDisplayName
        }

        override fun areContentsTheSame(
            oldItem: RecordDetailModel,
            newItem: RecordDetailModel
        ): Boolean {
            return oldItem == newItem
        }

    }
}