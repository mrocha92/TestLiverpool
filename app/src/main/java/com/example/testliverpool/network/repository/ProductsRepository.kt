package com.example.testliverpool.network.repository

import com.example.testliverpool.network.entities.ProductsResponse
import com.example.testliverpool.network.server.ApiClient.productsService
import retrofit2.Response

class ProductsRepository {
    suspend fun getProducts(query: String, pag: String): Response<ProductsResponse> {
        return productsService.getProductList(query, pag)
    }
}
