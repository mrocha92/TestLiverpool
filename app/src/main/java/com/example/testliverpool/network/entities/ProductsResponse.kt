package com.example.testliverpool.network.entities

data class ProductsResponse(
    val pageType: String,
    val plpResults: PlpResults,
    val status: Status
)