package com.example.testliverpool.network.entities

data class Status(
    val status: String,
    val statusCode: Int
)