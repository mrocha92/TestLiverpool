package com.example.testliverpool.network.entities

data class VariantsColor(
    val colorHex: String,
    val colorImageURL: String,
    val colorMainURL: Any,
    val colorName: String,
    val skuId: Any
)