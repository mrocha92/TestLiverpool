package com.example.testliverpool.network.remote

import com.example.testliverpool.network.entities.ProductsResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ProductsService {

    @GET("appclienteservices/services/v3/plp?")
    suspend fun getProductList(@Query("search-string")term:String, @Query("page-number")pag:String): Response<ProductsResponse>

}