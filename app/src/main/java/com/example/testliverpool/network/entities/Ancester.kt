package com.example.testliverpool.network.entities

data class Ancester(
    val categoryId: String,
    val label: String
)