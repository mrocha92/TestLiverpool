package com.example.testliverpool.network.entities

data class SortOption(
    val label: String,
    val sortBy: String
)