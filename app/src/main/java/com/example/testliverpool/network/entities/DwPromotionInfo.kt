package com.example.testliverpool.network.entities

data class DwPromotionInfo(
    val dWPromoDescription: String,
    val dwToolTipInfo: String
)