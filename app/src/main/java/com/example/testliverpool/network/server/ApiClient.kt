package com.example.testliverpool.network.server

import com.example.testliverpool.network.remote.ProductsService
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

const val API_ENDPOINT = "https://shoppapp.liverpool.com.mx/"

object ApiClient {

    private val restAdapter = Retrofit.Builder()
        .baseUrl(API_ENDPOINT)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    val productsService: ProductsService = restAdapter.create(ProductsService::class.java)

}