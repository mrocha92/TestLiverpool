package com.example.testliverpool.network.entities

data class PlpResults(
    val customUrlParam: CustomUrlParam,
    val label: String,
    val navigation: Navigation,
    val plpState: PlpState,
    val records: List<RecordModel>,
    val refinementGroups: List<RefinementGroup>,
    val sortOptions: List<SortOption>
)