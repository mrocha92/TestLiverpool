package com.example.testliverpool.network.entities

data class Current(
    val categoryId: String,
    val label: String
)