package com.example.testliverpool.view

import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.example.testliverpool.databinding.ActivityMainBinding
import com.example.testliverpool.view.adapters.ProductsPagingAdapter
import com.example.testliverpool.view.viewModels.ProductsViewModel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding
    private val viewModel: ProductsViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupView()
    }

    private fun setupView() {
        binding.apply {
            val productsAdapter = ProductsPagingAdapter {
                binding.progressBar.visibility = View.GONE
            }
            rvProducts.adapter = productsAdapter

            lifecycleScope.launch {
                viewModel.getProductsPagingSource("").collectLatest {
                    productsAdapter.submitData(it)
                }
            }

            search.etSearchService.setOnEditorActionListener { query, actionId, keyEvent ->

                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    binding.progressBar.visibility = View.VISIBLE
                    lifecycleScope.launch {
                        viewModel.getProductsPagingSource(query.text.toString()).collectLatest {
                            productsAdapter.submitData(it)
                        }
                    }
                    true
                } else {
                    false
                }

            }
        }
    }

}