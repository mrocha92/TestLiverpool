package com.example.testliverpool.view.viewModels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.testliverpool.usescases.usescases.GetProductsPagingSource

class ProductsViewModel : ViewModel() {


  private val getProductsPagingSource = GetProductsPagingSource()
fun getProductsPagingSource(query: String) =
  getProductsPagingSource.invoke(query, viewModelScope)

}