package com.example.testliverpool.view.adapters

import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.load
import coil.request.ImageRequest
import coil.transform.CircleCropTransformation
import com.example.testliverpool.R
import com.example.testliverpool.databinding.ItemProductBinding
import com.example.testliverpool.local.products.RecordDetailModel
import com.example.testliverpool.network.entities.VariantsColor


class ProductsPagingAdapter(private val listener: () -> Unit) :
    PagingDataAdapter<RecordDetailModel, ProductsPagingAdapter.ViewHolder>(
        RecordDetailModel.Comparator()
    ) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemProductBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        item?.let {
            holder.bind(item, listener)
        }
    }

    class ViewHolder(private val binding: ItemProductBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(itemData: RecordDetailModel, listener: () -> Unit) {
            binding.apply {
                listener.invoke()
                tittleProduct.text = itemData.productInfo.productDisplayName
                oldPrice.text =
                    binding.root.context.getString(
                        R.string.old_price,
                        itemData.productInfo.listPrice?.toString()
                    )

                newPrice.text =
                    binding.root.context.getString(
                        R.string.new_price,
                        itemData.productInfo.promoPrice?.toString()
                    )

                imgProduct.load(itemData.productInfo.lgImage) {
                    listener(onError = { _: ImageRequest, throwable: Throwable ->
                        Log.e("IMGERROR", throwable.message.toString())
                    })
                    crossfade(true)
                    placeholder(R.mipmap.ic_launcher)
                    error(R.mipmap.ic_launcher)
                    transformations(CircleCropTransformation())
                }

                val colorHexList = itemData.productInfo.variantsColor ?: listOf()

                if (colorHexList[0].colorHex.isNotEmpty()) {
                    binding.parentContainer.setBackgroundColor(Color.parseColor(colorHexList[0].colorHex))
                }

            }
        }
    }
}